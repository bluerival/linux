#!/bin/bash

TEMP_DIR='/tmp/awscli.install'

rm -rf "${TEMP_DIR}"
mkdir -p "${TEMP_DIR}"
cd "${TEMP_DIR}"

curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip
./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

cd
rm -rf "${TEMP_DIR}"


# to uninstall:
#
# $ sudo rm -rf /usr/local/aws
# $ sudo rm /usr/local/bin/aws