#!/bin/bash

# regular expression library
apt-get -y install libpcre3

# phantomjs dependency
apt-get -y install libfontconfig1