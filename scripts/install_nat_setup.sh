#!/bin/bash

TEMPFILE="/tmp/nat-setup"

function finish {
    rm -rf "${TEMPFILE}"
}
trap finish EXIT

wget --quiet -O "${TEMPFILE}" "https://bitbucket.org/bluerival/linux/raw/master/scripts/nat-setup?_no-cache=`date -u +%Y%m%dT%H%M%S`"

echo "Setting up nat services"
cp "${TEMPFILE}" /etc/network/if-pre-up.d/nat-setup
chmod ugo+x /etc/network/if-pre-up.d/nat-setup
/etc/network/if-pre-up.d/nat-setup
