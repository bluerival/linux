#!/bin/bash

apt-get -y install python3-pip
pip3 install --upgrade pip
pip3 install setuptools
pip3 install --upgrade --pre acdcli
mkdir -p /root/.cache/acd_cli
touch /root/.cache/acd_cli/oauth_data
mkdir -p /net/acd