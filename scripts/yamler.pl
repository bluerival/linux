#!/usr/bin/perl

use strict;
use warnings;

use YAML::Tiny;
use Data::Dumper;

# (1) quit unless we have the correct number of command-line args
my $num_args = $#ARGV + 1;
if ($num_args < 3 || $num_args > 4) {
    print "\nUsage: yamler.pl <file> <get|set> <field> [value]\n";
    exit 1;
}

my $filename = $ARGV[0];
if (! -e $filename) {
   print "Error: File " . $filename . " does not exists\n";
   exit 2;
}

my $command = $ARGV[1];
if ($command ne "get" && $command ne "set" ) {
        print "\nError: " . $command . " is not a supported command\n";
        exit 3;
}

my $field = $ARGV[2];

my $value = "";
if ($command eq "set") {
        $value = $ARGV[3];
}

# Open the config
my $yaml = YAML::Tiny->read( $filename );

# Get a reference to the first document
my $config = $yaml->[0];
my $configRef = $config;

my @fields = split(/\./, $field);

for my $i (0..$#fields) {
        $field = $fields[$i];
        if ( $i == $#fields ) {
                if ($command eq "set") {
                        $configRef->{$field} = $value;
                } else {
                        print $configRef->{$field};
                }
        } else {
                if (! exists $configRef->{$field}) {
                        $configRef->{$field} = {};
                }
                $configRef = $configRef->{$field};
        }
}

# Save the document back to the file
if ($command eq 'set') {
        $yaml->write( $filename );
}