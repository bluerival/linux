#!/bin/bash

SCRIPT_URL="${1}";

TMP_DIR=`mktemp -d -t br-admin.XXXXXXXXXX`;

if [ ! -d "${TMP_DIR}" ]; then
	echo "Temp directory does not exist: ${TMP_DIR}";
	exit 1;
fi

function finish {
  rm -rf "${TMP_DIR}";
}
trap finish EXIT;

echo "Using temporary directory ${TMP_DIR}";
cd "${TMP_DIR}";

echo "Acquiring ${SCRIPT_URL}";
curl --silent "${SCRIPT_URL}" -o run.sh;
EXIT_CODE="${?}";

if [ "${EXIT_CODE}" != "0" ]; then
	echo "Curl exited with ${EXIT_CODE}";
	exit 1;
fi

if [ `file -ib run.sh | sed 's/;.*//'` != 'text/x-shellscript' ]; then
 	echo "File is not a shell script";
 	exit 1;
fi

/bin/bash run.sh;
EXIT_CODE="${?}";

if [ "${?}" != "0" ]; then
	echo "Script exited with code ${EXIT_CODE}";
	exit "${EXIT_CODE}";
fi

finish;

