#!/bin/bash

echo "Installing ElasticSearch 2.x";

wget -qO - https://packages.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

rm -rf /etc/apt/sources.list.d/elasticsearch-2.x.list
echo "deb http://packages.elastic.co/elasticsearch/2.x/debian stable main" | sudo tee -a /etc/apt/sources.list.d/elasticsearch-2.x.list

sudo apt-get update
sudo apt-get -y install elasticsearch

sudo update-rc.d elasticsearch defaults 95 10

vi /etc/elasticsearch/elasticsearch.yml