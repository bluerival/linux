#!/bin/bash

echo "Installing MongoDB 3.4.x";

# only run install once
LOCK_FILE="/opt/br_install_mongodb_3.4.x_on_ubuntu.done";
if [ -f "${LOCK_FILE}" ]; then
		echo "Lock file exists. Not installing.";
		exit;
fi
touch "${LOCK_FILE}";

if [ "${MONGODB_DB_PATH}" == "" ]; then
		MONGODB_DB_PATH="/var/lib/mongodb";
fi
if [ "${MONGODB_LOG_PATH}" == "" ]; then
		MONGODB_LOG_PATH="/var/log/mongodb";
fi

echo "Using MONGODB_DB_PATH: ${MONGODB_DB_PATH}";
echo "Using MONGODB_LOG_PATH: ${MONGODB_LOG_PATH}";

TEMPYAMLER="/tmp/yamler.pl";
TEMPHUGEPAGES="/tmp/disable-transparent-hugepages-ubuntu";

function finish {
	rm -rf "${TEMPYAMLER}" "${TEMPHUGEPAGES}";
}
trap finish EXIT;

wget --quiet -O "${TEMPYAMLER}" "https://bitbucket.org/bluerival/linux/raw/master/scripts/yamler.pl?_no-cache=`date -u +%Y%m%dT%H%M%S`";
if [ ! -f "${TEMPYAMLER}" ]; then
		echo "Could not download yamler.pl utility.";
		exit 1;
fi
chmod u+x "${TEMPYAMLER}";

echo "Disabling hugepages.";
wget --quiet -O "${TEMPHUGEPAGES}" "https://bitbucket.org/bluerival/linux/raw/master/scripts/disable-transparent-hugepages-ubuntu?_no-cache=`date -u +%Y%m%dT%H%M%S`";
if [ ! -f "${TEMPHUGEPAGES}" ]; then
		echo "Could not download hugepages management script.";
		exit 1;
fi
chmod u+x "${TEMPHUGEPAGES}";

mv "${TEMPHUGEPAGES}" /etc/init.d/disable-transparent-hugepages;
update-rc.d disable-transparent-hugepages defaults;
/etc/init.d/disable-transparent-hugepages start

echo "Configuring apt.";
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
DISTRO=`lsb_release -a 2>&1 | grep Release | sed -E 's/^Release:\s+//'`;
rm -rf /etc/apt/sources.list.d/mongodb-org-3.4.list;
if [ "${DISTRO}" == "16.04" ]; then
		echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu precise/mongodb-org/3.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list;
elif [ "${DISTRO}" == "14.04" ]; then
		echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list;
elif [ "${DISTRO}" == "12.04" ]; then
		echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu precise/mongodb-org/3.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list;
else
		echo "Unsupported Ubuntu version: ${DISTRO}";
		exit 1;
fi

echo "Installing mongodb server and config editing tools";
apt-get update;
apt-get install -y -o=Dpkg::Use-Pty=0 mongodb-org libyaml-tiny-perl;

echo "Updating mongodb server config";
stop mongod;

$TEMPYAMLER /etc/mongod.conf set storage.dbPath "${MONGODB_DB_PATH}" >> /dev/null;
$TEMPYAMLER /etc/mongod.conf set systemLog.path "${MONGODB_LOG_PATH}/mongod.log" >> /dev/null;
#$TEMPYAMLER /etc/mongod.conf set storage.engine "wiredTiger" >> /dev/null;

if [ "${MONGODB_REPLICA_SET}" != "" ]; then
		$TEMPYAMLER /etc/mongod.conf set replication.replSetName "${MONGODB_REPLICA_SET}" >> /dev/null;
fi

if [ "${MONGODB_BINDIP}" != "" ]; then
		$TEMPYAMLER /etc/mongod.conf set net.bindIp "${MONGODB_BINDIP}" >> /dev/null;
fi

echo "Putting the DB path and Log path in place";
if [ -d /var/lib/mongodb ] && [ ! -d "${MONGODB_DB_PATH}" ]; then

		echo "Moving data directory /var/lib/mongodb->${MONGODB_DB_PATH}"

		mkdir -p "${MONGODB_DB_PATH}";
		rm -rf "${MONGODB_DB_PATH}";

		mv /var/lib/mongodb "${MONGODB_DB_PATH}";

else
		echo "Data directory already in place.";
fi

if [ -d /var/log/mongodb ] && [ ! -d "${MONGODB_LOG_PATH}" ]; then

		echo "Moving log directory /var/log/mongodb->${MONGODB_LOG_PATH}"

		mkdir -p "${MONGODB_LOG_PATH}";
		rm -rf "${MONGODB_LOG_PATH}";

		mv /var/log/mongodb "${MONGODB_LOG_PATH}";

else
		echo "Log directory already in place.";
fi

echo "Ensure permissions on data and log directories.";
chown -R mongodb:mongodb "${MONGODB_DB_PATH}" "${MONGODB_LOG_PATH}";
chmod -R ug+rwX "${MONGODB_DB_PATH}" "${MONGODB_LOG_PATH}";

echo "Finished installing and configuring MongoDB";
start mongod;