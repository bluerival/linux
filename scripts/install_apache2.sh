#!/bin/bash

apt-get -y install apache2

# stop apache
/etc/init.d/apache2 stop

# configure document root in apache config
sed -i '/DocumentRoot \/var\/www\/html/{s/.*/\tDocumentRoot \/opt\/var\/www\/html/}' /etc/apache2/sites-available/000-default.conf
sed -i '/DocumentRoot/{s/.*/&\n\n\t<Directory \/opt\/var\/www\/html>\n\t\tOptions -Indexes +FollowSymLinks +MultiViews\n\t\tAllowOverride All\n\t\tRequire all granted\n\t<\/Directory>/;:a;n;ba}' /etc/apache2/sites-available/000-default.conf
sed -i '/DocumentRoot \/var\/www\/html/{s/.*/\tDocumentRoot \/opt\/var\/www\/html/}' /etc/apache2/sites-available/default-ssl.conf
sed -i '/DocumentRoot/{s/.*/&\n\n\t<Directory \/opt\/var\/www\/html>\n\t\tOptions -Indexes +FollowSymLinks +MultiViews\n\t\tAllowOverride All\n\t\tRequire all granted\n\t<\/Directory>/;:a;n;ba}' /etc/apache2/sites-available/default-ssl.conf

# ensure document root exists with correct permissions
mkdir -p /opt/var/www/html
chown -R www-data:www-data /opt/var/www/html
chmod -R ug+rwX /opt/var/www/html

# enable modules
a2enmod rewrite

#enable SSL site
a2enmod ssl
cd /etc/apache2/sites-available
a2ensite default-ssl.conf

# start apache again
/etc/init.d/apache2 start
