#!/bin/bash

ZABBIX_CONF_FILE="/etc/zabbix/zabbix_server.conf"
PHP_CONF_FILE="/etc/apache2/conf-enabled/zabbix.conf"

# ensure no previous install
apt-get update
apt-get -y purge mysql-server-core-5.5 mysql-server-5.5 zabbix-server-mysql zabbix-frontend-php
rm -rf "${ZABBIX_CONF_FILE}" "${PHP_CONF_FILE}" /var/log/zabbix

# zabbix-server-mysql is going to force an install of mysql server. it will prompt for a password
# this automated script will supply a dummy password so the script doesn't time out.
# Of course, we are going to remove mysql server later as we plan to use an external mysql instance
# anyway, which makes this all a hack.
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password your_password'
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password your_password'
apt-get -y install mysql-server-core-5.5 mysql-server-5.5

apt-get -y install zabbix-server-mysql zabbix-frontend-php

# We are going to use an external mysql server, so disable this one
service mysql stop
update-rc.d -f mysql remove

# prepare to configure these
service apache2 stop
service zabbix-server stop

function setZabbixConfigValue {
	sed -i.$1.bak -r "s/^(\s*)#? ?${1}=.*/\\1${1}=${2}/" "${ZABBIX_CONF_FILE}"
}

function setPHPConfigValue {
	sed -i.$1.bak -r "s/^(\s*)#? ?php_value ${1} .*/\\1php_value ${1} ${2}/" "${PHP_CONF_FILE}"
}

setZabbixConfigValue "DBHost" "${ZABBIX_DB_HOST}"
setZabbixConfigValue "DBName" "${ZABBIX_DB_NAME}"
setZabbixConfigValue "DBUser" "${ZABBIX_DB_USER}"
setZabbixConfigValue "DBPassword" "${ZABBIX_DB_PASSWORD}"

setPHPConfigValue "date.timezone" "Etc\\/UTC"

# all configured, start up again
service apache2 start
service zabbix-server start