#!/bin/bash

CODENAME=`cat /etc/lsb-release | grep DISTRIB_CODENAME | sed 's/DISTRIB_CODENAME=//'`

# ensure no previous install
apt-get -y purge zabbix-release
apt-get update

cd /tmp
wget "http://repo.zabbix.com/zabbix/3.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_3.0-1+${CODENAME}_all.deb"
dpkg -i "zabbix-release_3.0-1+${CODENAME}_all.deb"
rm -rf "zabbix-release_3.0-1+${CODENAME}_all.deb"

apt-get update