#!/bin/bash

TEMPFILE="/tmp/root_bashrc_default"

function finish {
    rm -rf "${TEMPFILE}"
}
trap finish EXIT

function setFile () {
	wget --quiet -O "${TEMPFILE}" "https://bitbucket.org/bluerival/linux/raw/master/profiles/root/${1}"

	echo "Updating root profile. Setting /root/${2}"
	cp "${TEMPFILE}" "/root/${2}"
}


setFile "_.bashrc.sh" ".bashrc"
setFile "_.vimrc" ".vimrc"


[ -d "/root/downloads" ] || mkdir -p "/root/downloads"