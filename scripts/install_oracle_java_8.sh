#!/bin/bash

apt-get -y install python-software-properties
add-apt-repository -y ppa:webupd8team/java

apt-get update
apt-get -y install oracle-java8-installer