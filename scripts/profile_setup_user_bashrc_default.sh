#!/bin/bash

TEMPFILE="/tmp/user_bashrc_default"

function finish {
    rm -rf "${TEMPFILE}"
}
trap finish EXIT

function setFile () {

	wget --quiet -O "${TEMPFILE}" "https://bitbucket.org/bluerival/linux/raw/master/profiles/user/${1}"

	echo "Setting /etc/skel/${2}"
	cp "${TEMPFILE}" "/etc/skel/${2}"

	for USER in `ls /home/`
	do
		HOMEDIR="/home/${USER}"
		DESTINATION="${HOMEDIR}/${2}"
		echo "Setting ${DESTINATION}"
		cp "${TEMPFILE}" "${DESTINATION}"
		chown -R "${USER}:${USER}" "${HOMEDIR}"
	done

}

setFile "_.bashrc.sh" ".bashrc"
setFile "_.vimrc" ".vimrc"


echo "Creating directory /etc/skel/downloads"
[ -d "/etc/skel/downloads" ] || mkdir -p "/etc/skel/downloads"
for USER in `ls /home/`
do
	HOMEDIR="/home/${USER}"
	echo "Creating directory ${HOMEDIR}/downloads"
	[ -d "${HOMEDIR}/downloads" ] || mkdir -p "${HOMEDIR}/downloads"
done