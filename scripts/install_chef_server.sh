#!/bin/bash

PKG_FILE_URL="https://packagecloud.io/chef/stable/packages/ubuntu/trusty/chef-server-core_12.4.1-1_amd64.deb/download"
PKG_FILE_SHA1="a75e8dbcce749adf61a60ca0ccf25fc041e4774a"

ADMIN_USERNAME="admin"
ADMIN_PASSWORD="PASSWORDME!"
ADMIN_EMAIL="admin@bluerival.com"
ADMIN_FIRST_NAME="Admin"
ADMIN_LAST_NAME="User"

ORGANIZATION_NAME_SHORT="bluerival"
ORGANIZATION_NAME_FULL="BlueRival Software"

##############################################################################

if [ `whoami` != 'root' ]; then
	sudo $0
	exit
fi

cd /tmp

PKG_FILE="chef_core.deb"
SHA1_SUM=`[ -f "${PKG_FILE}" ] && sha1sum ${PKG_FILE}`

if [ -f "${PKG_FILE}" ] && [ "${SHA1_SUM}" == "${PKG_FILE_SHA1}  ${PKG_FILE}" ]; then
	echo -n #no-op
else
	rm -rf "${PKG_FILE}"
	wget -O "${PKG_FILE}" "${PKG_FILE_URL}"
fi

SHA1_SUM=`[ -f "${PKG_FILE}" ] && sha1sum ${PKG_FILE}`
if [ -f "${PKG_FILE}" ] && [ "${SHA1_SUM}" == "${PKG_FILE_SHA1}  ${PKG_FILE}" ]; then
	echo "Downloaded ${PKG_FILE_URL}"
else
	echo "Failed to download ${PKG_FILE_URL}"
	exit
fi

# install base server package
apt-get update
dpkg -i "${PKG_FILE}"
chef-server-ctl reconfigure

# add an admin user
chef-server-ctl user-create "${ADMIN_USERNAME}" "${ADMIN_FIRST_NAME}" "${ADMIN_LAST_NAME}" "${ADMIN_EMAIL}" "${ADMIN_PASSWORD}" --filename "~/${ADMIN_USERNAME}.user.rsa.pem"

# add the base organization
chef-server-ctl org-create "${ORGANIZATION_NAME_SHORT}" "${ORGANIZATION_NAME_FULL}" --association_user "${ADMIN_USERNAME}" --filename "~/${ORGANIZATION_NAME_SHORT}.organization.rsa.pem"

# install Chef Manage web UI
chef-server-ctl install chef-manage
chef-server-ctl reconfigure
chef-manage-ctl reconfigure

# install Chef Push Jobs
chef-server-ctl install opscode-push-jobs-server
chef-server-ctl reconfigure
opscode-push-jobs-server-ctl reconfigure

# install Chef Reporting
chef-server-ctl install opscode-reporting
chef-server-ctl reconfigure
opscode-reporting-ctl reconfigure