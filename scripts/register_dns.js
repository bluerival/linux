'use strict';

var __ = require( 'doublescore' );

var async = require( 'async' );
var AWS = require( 'aws-sdk' );
var os = require( 'os' );

var internalIp = process.env.SCALR_INTERNAL_IP ? process.env.SCALR_INTERNAL_IP : false;
var hostname = os.hostname();
var privateDomain = process.env.PRIVATE_DOMAIN ? process.env.PRIVATE_DOMAIN.replace( /\.*$/, '.' ) : false;

if ( !internalIp || !internalIp.match( /^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/ ) ) {
	console.error( 'internal ip not valid' );
	process.exit();
}

if ( !hostname ) {
	console.error( 'hostname not set' );
	process.exit();
}

if ( !privateDomain ) {
	console.error( 'PRIVATE_DOMAIN not set' );
	process.exit();
}

var ipParts = internalIp.split( '.' );
var dnsEntry = hostname + '.' + privateDomain;
console.error( 'dnsEntry', dnsEntry );

var route53 = new AWS.Route53( {
	apiVersion: '2013-04-01',
	region: 'us-east-1'
} );

async.waterfall( [
		( done ) => {
			route53.listHostedZonesByName( {}, done );
		},
		( zones, done ) => {

			zones = __( {
				HostedZones: []
			} ).mixin( zones );

			if ( !__.isArray( zones.HostedZones ) ) {
				return done( new Error( 'no results' ) );
			}

			let zoneIds = {
				ptr: null,
				internal: null
			};

			zones.HostedZones.forEach( ( zone ) => {
				if ( zone.Name === '10.in-addr.arpa.' ) {
					zoneIds.ptr = zone.Id;
				} else if ( zone.Name === privateDomain ) {
					zoneIds.internal = zone.Id;
				}
			} );

			if ( !zoneIds.internal || !zoneIds.ptr ) {
				return done( new Error( 'domain not found' ) );
			}

			done( null, zoneIds );

		},
		( zoneIds, done ) => {

			async.series( [

				( done ) => {
					route53.changeResourceRecordSets( {
						HostedZoneId: zoneIds.internal,
						ChangeBatch: {
							Changes: [
								{
									Action: 'UPSERT',
									ResourceRecordSet: {
										Name: dnsEntry,
										Type: 'A',
										ResourceRecords: [
											{
												Value: internalIp
											}
										],
										TTL: 60
									}
								}
							]
						}
					}, done );
				},

				( done ) => {
					route53.changeResourceRecordSets( {
						HostedZoneId: zoneIds.ptr,
						ChangeBatch: {
							Changes: [
								{
									Action: 'UPSERT',
									ResourceRecordSet: {
										Name: ipParts[ 3 ] + '.' + ipParts[ 2 ] + '.' + ipParts[ 1 ] + '.10.in-addr.arpa.',
										Type: 'PTR',
										ResourceRecords: [
											{
												Value: dnsEntry
											}
										],
										TTL: 60
									}
								}
							]
						}
					}, done );

				} ], done );

		}
	],
	( err, data ) => {

		if ( err ) {
			console.error( 'Error:', err );
		} else {
			console.error( 'Response:', err, JSON.stringify( data, null, 4 ) );
		}

		process.exit();

	} );