#!/bin/bash

cd /tmp
wget "https://download.code42.com/installs/linux/install/CrashPlan/CrashPlan_4.7.0_Linux.tgz"
tar -xzf CrashPlan_4.7.0_Linux.tgz
cd crashplan-install

# modify installer to be a batch installer
sed -i 's/^\s*read.*//' install.sh
sed -i 's/more \.\/EULA\.txt//' install.sh
sed -i 's/agreed=0/agreed=1/' install.sh

./install.sh

cd /
rm -rf /tmp/CrashPlan_4.7.0_Linux.tgz
rm -rf /tmp/crashplan-install