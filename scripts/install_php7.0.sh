#!/bin/bash

PHP_VER='7.0'

add-apt-repository -y ppa:ondrej/php

apt-get update

# install php$PHP_VER and all common libraries that go with it
apt-get -y install php$PHP_VER libapache2-mod-php$PHP_VER php-common php$PHP_VER-cli php$PHP_VER-common

# ensure the module was enabled in apache2
a2enmod php$PHP_VER

function install_package () {
	echo "Start installing package: ${1}"
	apt-get -y install "${1}"
	echo "Finished installing package: ${1}"
}

install_package php$PHP_VER-mysql
install_package php$PHP_VER-json
install_package php$PHP_VER-opcache
install_package php$PHP_VER-readline
install_package php$PHP_VER-curl
install_package php$PHP_VER-gd
install_package php$PHP_VER-intl
install_package php$PHP_VER-mbstring
install_package php$PHP_VER-xml
install_package php$PHP_VER-mcrypt
install_package php$PHP_VER-bcmath
install_package php$PHP_VER-bz2
install_package php$PHP_VER-imap
install_package php$PHP_VER-ldap
install_package php$PHP_VER-soap
install_package php$PHP_VER-sqlite3
install_package php$PHP_VER-tidy
install_package php$PHP_VER-xmlrpc
install_package php$PHP_VER-zip

function set_config_file () {

	# prepare value for use in sed replacement
	VALUE=`echo "${3}" | sed 's/\//\\\\\//g'`

	sed "-i.${2}.bak" -r "s/^\s*;?\s*${2}\s*=.*/${2} = ${VALUE}/" "${1}"

}

function set_config () {

	set_config_file /etc/php/$PHP_VER/apache2/php.ini $1 $2
	set_config_file /etc/php/$PHP_VER/cli/php.ini $1 $2

}

set_config post_max_size 60M
set_config upload_max_filesize 60M
set_config default_socket_timeout 600
set_config max_execution_time 600
set_config max_input_time 600
set_config memory_limit 512M
set_config date.timezone "UTC"
#set_config error_log /tmp/php_errors.log

/etc/init.d/apache2 restart