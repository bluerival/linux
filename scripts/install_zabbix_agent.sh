#!/bin/bash


ZABBIX_CONF_FILE="/etc/zabbix/zabbix_agentd.conf"

function setZabbixConfigValue {
	sed -i.$1.bak -r "s/^(\s*)#? ?${1}=.*/\\1${1}=${2}/" "${ZABBIX_CONF_FILE}"
}

function clearZabbixConfigValue {
	sed -i.$1.clear.bak -r "s/^(\s*)#? ?${1}=.*/\\1# ${1}=/" "${ZABBIX_CONF_FILE}"
}


# ensure no previous install
apt-get update
service zabbix-agent stop
apt-get -y purge zabbix-agent
rm -rf /etc/zabbix /var/log/zabbix-agent

apt-get -y install zabbix-agent
service zabbix-agent stop

# hard coded meta data
clearZabbixConfigValue "HostMetadataItem"

setZabbixConfigValue "Hostname" "`hostname`.${PRIVATE_DOMAIN}"
setZabbixConfigValue "Server" "${ZABBIX_SERVER_HOST}"
setZabbixConfigValue "ServerActive" "${ZABBIX_SERVER_HOST}"
setZabbixConfigValue "HostMetadata" "Linux ${ZABBIX_AGENT_SECRET_KEY}"

[[ -x /bin/systemctl ]] && /bin/systemctl enable zabbix-agent # setup systemv daemon
service zabbix-agent start